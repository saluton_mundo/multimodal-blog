<!DOCTYPE html>
<html lang="en-GB">
<head>
	<!-- Project Site metadata -->
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta name="description" content="Research site for Web-based multimodal human computer interaction">
	<meta name="keywords" content="multimodal, speech, gesture, javascript, HCI, Web 2.0, user interface, UX">
	<meta name="author" content="John Nolan, Student 1417604, part of 3rd year project BSc Computing Solutions (Internet), Colchester Institute.">
	<meta name="copyright" content="2015 John Nolan">
	<!-- Stylesheet includes -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="css/material.css">
	<link rel="stylesheet" type="text/css" href="css/material-wfont.css">
	<link rel="stylesheet" type="text/css" href="css/ripples.css">
	<link rel="stylesheet" type="text/css" href="css/site.css">
	<title>Multimodal UX</title>
</head>
<body>
	<div class="bs-component" style="position: fixed; width: 100%; z-index: 999; border-bottom: 4px solid; border-color:#ffd20d;">
        <div class="navbar navbar-default">
        	<div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand logo" href="index.php"> multimodal</a>
                </div>
                <a href="javascript:void(0)" id="center-mic" class="btn btn-default btn-fab btn-raised mdi-av-mic-none" style="position: absolute; margin: auto; float: none; left: 50%; width: 100px; height: 100px; font-size: 70px; color: cadetblue; top: 20%; background: floralwhite;"></a>
                <div class="navbar-collapse collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="bootstrap-elements.html" data-target="#" class="dropdown-toggle" data-toggle="dropdown"><span class="mdi-navigation-menu big_icon"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="javascript:void(0)">What is Multimodal?</a></li>
                                <li><a href="javascript:void(0)">Read more</a></li>
                                <li><a href="javascript:void(0)">W3C Working Groups</a></li>
                                <li><a href="javascript:void(0)">Settings</a></li>
                                <li class="divider"></li>
                                <li style="color:#ebebeb;"><center>Copyright &copy; J.Nolan</center></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="wide"></div>
	<!-- Page content here -->
