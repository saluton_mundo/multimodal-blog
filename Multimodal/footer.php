			<div id="audio"></div><!-- container for dynamic HTML audio element -->
		<!-- JavaScript Includes -->
		<script src="js/jquery-1.11.2.min.js"></script>
		<script src="js/bootstrap.js"></script>
		<script src="js/material.js"></script>
		<script src="js/bootstrap-switch.js"></script>
		<script src="js/ripples.js"></script>
		<script src="js/responsiveVoice.js"></script>
		<script src="js/spin.js"></script>
		<script src="js/annyang.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {

				// Function to pass post data to speech synthesis
				$(".read").click(function () {
					$(this).hide();
					$(this).next(".stop").show();
					var id = $(this).attr("id")
					//var text = $(this).next(".article").text();
					var text = document.getElementById("article"+id).innerHTML;
					responsiveVoice.speak(text);
				});
				// Function to stop the speech synthesis
				$(".stop").click(function () {
					$(this).hide();
					$(this).prev(".read").show();
					responsiveVoice.cancel();
				});
				function getSelectionText() {
				    var text = "";
				    if (window.getSelection) {
				        text = window.getSelection().toString();
				    } 
				    return text;
				}
				
				// Variables
				var recognizing = false;
				var ignore_onend;
				var start_timestamp;
				var commands = [ "read", "stop", "comment", "share" ];
				var social = [ "facebook", "twitter", "google", "stumbleupon" ];

				// Create Speech Recognition
				var recognizer = new webkitSpeechRecognition();
				recognizer.continuous = true;
				recognizer.interimResults = true;

				// Create Spinner 
				var spinner = new Spinner({
					lines: 12, // The number of lines to draw
					length: 7, // The length of each line
					width: 5, // The line thickness
					radius: 24, // The radius of the inner circle
					color: '#009587', // color of the spinner
					speed: 1, // Rounds per second
					trail: 100, // Afterglow percentage
					shadow: true // Whether to render a shadow
				});

				console.log("Speech recognition is good to go!");

				// Invoke Speech Recognition
				$('#center-mic').click(function() {
					
					$(this).toggleClass("mdi-av-mic-none");
					//responsiveVoice.speak("What should I do?");
								
					if(!recognizing) {
						recognizer.start();	
						recognizing = true;
						console.log("Speech recognition Started!");
						spinner.spin(document.getElementById("center-mic"));
					}
					else {
						recognizer.stop();
						recognizing = false;
						console.log("Speech recognition Ended!");
						spinner.stop();
					}
				});
				$('p, h1, h2, h4').mouseup(function (e){
				    responsiveVoice.speak(getSelectionText());
				});

				if (annyang) {
					// Let's define our first command. First the text we expect, and then the function it should call
					var commands = {
						'hello': function() {
							responsiveVoice.speak("hi there");
						},
						'down': function() {
							responsiveVoice.speak("Moving down!");
							var y = $(window).scrollTop();  //your current y position on the page
							$(window).scrollTop(y+475);
						},
						'up': function() {
							responsiveVoice.speak("Moving up!");
							var x = $(window).scrollTop();
							$(window).scrollTop(x-475);
						},
						'comment': function() {
							// function to handle commenting
						},
						'stop': function() {
							responsiveVoice.cancel();
						}
					};
					// Add our commands to annyang
					annyang.addCommands(commands);
					// Start listening. You can call this here, or attach this call to an event, button, etc.
					annyang.start();
					console.log("Miss Annyang is good to go!");
				}
			});
		</script>
	</body>
</html>