<?php include("header.php") ?>
<?php require('php/functions.php') ?>
		<div class="container">
			<div class="col-lg-8 col-md-8 col-sm-8">
				<?php display_posts(); ?>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-4" style="margin-top:10px;">
				<div class="panel panel-primary">
				    <div class="panel-heading"><h3 class="panel-title">Useful Tips.</h3></div>
				    <div class="panel-body">
				    	<div class="list-group">
						    <div class="list-group-item">
						        <div class="row-action-primary">
						            <i class="mdi-communication-textsms" style="margin-top: 15px;"></i>
						        </div>
						        <div class="row-content">
						        	<div class="least-content"></div>
						            <h4 class="list-group-item-heading">Speech Control</h4>
						            <p class="list-group-item-text">You can control the blog using your speech. Just enable access to your mic.</p>
						        </div>
						    </div>
						    <div class="list-group-separator"></div>
						    <div class="list-group-item">
						        <div class="row-action-primary">
						            <i class="mdi-av-hearing" style="margin-top: 15px;"></i>
						        </div>
						        <div class="row-content">
						        	<div class="least-content"></div>
						            <h4 class="list-group-item-heading">Speech Synthesis</h4>
						            <p class="list-group-item-text">When you click the "Read it to me" button I will read you the article. Highlighting text will cause me to read it.</p>
						        </div>
				    		</div>
				    	</div>
				    </div>
				</div>
				<div class="panel panel-primary">
				    <div class="panel-heading">
				        <h3 class="panel-title">Getting Started.</h3>
					</div>
					<div class="panel-body">
				    	<div class="list-group">
							<div class="list-group-item">
						        <div class="row-action-primary">
						            <i class="mdi-communication-chat" style="margin-top: 15px;"></i>
						        </div>
						        <div class="row-content">
						        	<div class="least-content"></div>
						            <h4 class="list-group-item-heading">"Read"</h4>
									<p class="list-group-item-text">Highlight text to get the AI to read it to you</p>	
						        </div>
				    		</div>
				    		<div class="list-group-separator"></div>
							<div class="list-group-item">
						        <div class="row-action-primary">
						            <i class="mdi-av-not-interested" style="margin-top: 15px;"></i>
						        </div>
						        <div class="row-content">
						        	<div class="least-content"></div>
						            <h4 class="list-group-item-heading">"Stop"</h4>
					        		<p class="list-group-item-text">This will cancel the last voice command. Also stops AI reading a post.</p>
						        </div>
				    		</div>
				    		<div class="list-group-separator"></div>
				    		<div class="list-group-item">
						        <div class="row-action-primary">
						            <i class="fa fa-comment" style="margin-top: 15px;"></i>
						        </div>
						        <div class="row-content">
						        	<div class="least-content"></div>
						            <h4 class="list-group-item-heading">"Comment"</h4>
					        		<p class="list-group-item-text">Saying comment whilst hovering the cursor over a post to reply to the article.</p>							   
				    			</div>
				    		</div>
				    		<div class="list-group-separator"></div>
							<div class="list-group-item">
						        <div class="row-action-primary">
						            <i class="mdi-social-share" style="margin-top: 15px;"></i>
						        </div>
						        <div class="row-content">
						        	<div class="least-content"></div>
									<h4 class="list-group-item-heading">"Share"</h4>
					      			<p class="list-group-item-text">Saying share then the social platform. For example, "Share Facebook" to share the post to Facebook.</p>				    			
					      		</div>
				    		</div>	        
				    	</div>
			    	</div>
				</div>
			</div>		
		</div>
<?php include("footer.php") ?>