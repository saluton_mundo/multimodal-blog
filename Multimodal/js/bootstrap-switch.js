(function ( $ ) {

	$.fn.bootstrapSwitch = function( options ) {

		var settings = $.extend({
			on: 'On',
			off: 'Off	',
			onLabel: '&nbsp;&nbsp;&nbsp;',
			offLabel: '&nbsp;&nbsp;&nbsp;',
			same: false,//same labels for on/off states
			size: 'md',
			onClass: 'primary',
			offClass: 'default'
			}, options );

		settings.size = ' btn-'+settings.size;
		if (settings.same){
			settings.onLabel = settings.on;
			settings.offLabel = settings.off;
		}

		return this.each(function(e) {
			var c = $(this);
			var disabled = c.is(":disabled") ? " disabled" : "";

			var div = $('<div class="btn-group btn-toggle" style="white-space: nowrap;"></div>').insertAfter(this);
			var on = $('<button class="btn btn-primary '+settings.size+disabled+'" style="float: none;display: inline-block;"></button>').html(settings.on).css('margin-right', '0px').appendTo(div);
			var off = $('<button class="btn btn-danger '+settings.size+disabled+'" style="float: none;display: inline-block;"></button>').html(settings.off).css('margin-left', '0px').appendTo(div);

			function applyChange(b) {
				if(b) {
					on.attr('class', 'btn active btn-' + settings.onClass+settings.size+disabled).html(settings.on).blur();
					off.attr('class', 'btn btn-default '+settings.size+disabled).html(settings.offLabel).blur();
				}
				else {
					on.attr('class', 'btn btn-default '+settings.size+disabled).html(settings.onLabel).blur();
					off.attr('class', 'btn active btn-' + settings.offClass+settings.size+disabled).text(settings.off).blur();
				}
			}
			applyChange(c.is(':checked'));

			on.click(function(e) {e.preventDefault();c.prop("checked", !c.prop("checked")).trigger('change')});
			off.click(function(e) {e.preventDefault();c.prop("checked", !c.prop("checked")).trigger('change')});

			$(this).hide().on('change', function() {
				applyChange(c.is(':checked'))
			});
		});
	};
} ( jQuery ));