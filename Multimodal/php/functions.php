<?php
	function display_posts() {
	
	$servername = "localhost";
	$username = "root";
	$password = "";
	$db = "multimodal";

	// Create connection
	$conn = new mysqli($servername, $username, $password, $db);

	// Check connection
	if ($conn->connect_error) {
	    die("Connection failed: " . $conn->connect_error);
	} 

		$query = 'SELECT * FROM `posts` ORDER BY `post_date` DESC';
		$result = mysqli_query($conn, $query);

		if ($result != false) {
      		while ($a = mysqli_fetch_assoc($result)) {
      			// Place return DB data into PHP variables
      			$id = stripslashes($a["post_id"]);
        		$title = stripslashes($a["title"]);
        		$bodytext = stripslashes($a["body"]);
        		$img = $a["img"];
        		$date = stripcslashes($a["post_date"]);
        		// Render the blog post
        		echo "<div class='well row' style='margin-top:10px;' id='".$id."'><h1>".$title."</h1><a class='btn btn-success btn-raised read' id='".$id."'>Read it to me</a><a class='btn btn-danger btn-raised stop' style='display:none;'>Stop Reading</a><br /><b><u>Share:</u></b> ";
        		echo "<!-- Twitter --><a href='http://twitter.com/home?status=' title='Share on Twitter' target='_blank' class='btn btn-twitter'><i class='fa fa-twitter'></i> Twitter</a>";
        		echo "<!-- Facebook --><a href='https://www.facebook.com/sharer/sharer.php?u=' title='Share on Facebook' target='_blank' class='btn btn-facebook'><i class='fa fa-facebook'></i> Facebook</a>";
				echo "<!-- Google+ --><a href='https://plus.google.com/share?url=' title='Share on Google+' target='_blank' class='btn btn-googleplus'><i class='fa fa-google-plus'></i> Google+</a>";
				echo "<!-- StumbleUpon --><a href='http://www.stumbleupon.com/submit?url=' title='Share on StumbleUpon' target='_blank' data-placement='top' class='btn btn-stumbleupon'><i class='fa fa-stumbleupon'></i> Stumbleupon</a>";
				echo "<img src='".$img."' style='width:100%;' /><br /><br /><p id='article".$id."'>".$bodytext."</p>";
        		echo "<br /><p><i>Posted on ".$date."</i><a href='javascript:void(0)' class='pull-right'><span class='fa fa-comment'></span> Comment</a></p></div><br />";
        		// Once function defined call it to see comments relating to blog post
        		// display_comments();
        	}
        } else {
        	echo 'No entries in the blog yet';
        }
        mysqli_free_result($result);
        mysqli_close($conn);
	}

	/* FUNCTIONALITY TO DO  */
	function create_post() {}
	function display_comments() {}
	function create_comment() {}
?>